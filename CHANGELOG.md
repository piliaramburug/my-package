# CHANGELOG



## v0.2.1 (2024-04-06)

### Fix

* fix(deploy): add deploy pipeline ([`a018f17`](https://gitlab.com/piliaramburu/my-package/-/commit/a018f1749129f7818be3ed8aa5fc0cf252b23350))


## v0.2.0 (2024-04-06)

### Feature

* feat(test): add new test module ([`2120382`](https://gitlab.com/piliaramburu/my-package/-/commit/2120382aa3ade9aee868e883aa8f5e13254a04d2))


## v0.1.0 (2024-04-06)

### Feature

* feat(semantic-release): add semantic release to the repository ([`159934c`](https://gitlab.com/piliaramburu/my-package/-/commit/159934c59d2033e2cf9c8929857df5f1dbfead92))

### Unknown

* add semantic_release config ([`b57a390`](https://gitlab.com/piliaramburu/my-package/-/commit/b57a3902ba73059366b78d3e5d141341d40e21a8))

* add semantic release as dependency ([`2f1df92`](https://gitlab.com/piliaramburu/my-package/-/commit/2f1df92520a4a8c4c5470b6f73395a5ee8ffce0b))

* add pyproject.toml file ([`5ecc30f`](https://gitlab.com/piliaramburu/my-package/-/commit/5ecc30fcb15e3331d807879e41cb6c20eabfe028))

* update pipeline ([`c4a8d2f`](https://gitlab.com/piliaramburu/my-package/-/commit/c4a8d2f3b9370c1f89ac6ffb361b8f292cff9900))

* Add first pipeline ([`91ca0b1`](https://gitlab.com/piliaramburu/my-package/-/commit/91ca0b1ff9a8482c481dea1da795e8128faf4d9a))

* Initial commit ([`a7c3d18`](https://gitlab.com/piliaramburu/my-package/-/commit/a7c3d185ec1e4a6b7f9fd75b68fb43f3ed504fe4))
